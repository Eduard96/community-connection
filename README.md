0.Set server port (export CC_SERVER_PORT=<port>)

1.Database configuration (application.properties 20-21)
- set host (export CC_DATASOURCE_HOST=<host>)
- set port (export CC_DATASOURCE_PORT=<port>)
- create MySQL Database User if necessary
- set username (export CC_DATASOURCE_USERNAME=<username>)
- set password (export CC_DATASOURCE_PASSWORD=<password>)

2.Mail sending configuration (application.properties 51-52)
- set username (export CC_SUPPORT_MAIL_USERNAME=<username>)
- set password (export CC_SUPPORT_MAIL_PASSWORD=<password>)

3.Logging (application.properties 29)
- Uncomment (Thanks Cap)
- Maybe will be need to create folder by specified path
- set necessary permissions for the .log file 