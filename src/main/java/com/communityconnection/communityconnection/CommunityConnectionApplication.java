package com.communityconnection.communityconnection;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@EnableAsync
@SpringBootApplication
public class CommunityConnectionApplication {

	public static void main(String[] args) {
		SpringApplication.run(CommunityConnectionApplication.class, args);
	}

}
