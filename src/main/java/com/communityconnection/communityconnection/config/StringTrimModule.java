package com.communityconnection.communityconnection.config;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdScalarDeserializer;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Set;

@Component
public class StringTrimModule extends SimpleModule {

    private final static Set<String> ignoreTrim = Set.of(
            "login",
            "password",
            "newPassword1",
            "newPassword2",
            "oldPassword",
            "repeatedPassword"
    );

    public StringTrimModule() {
        addDeserializer(String.class, new StdScalarDeserializer<>(String.class) {
            @Override
            public String deserialize(JsonParser jsonParser, DeserializationContext ctx) throws IOException {
                if (ignoreTrim.contains(jsonParser.currentName())) {
                    return jsonParser.getValueAsString();
                }
                return jsonParser.getValueAsString().strip();
            }
        });
    }
}
