package com.communityconnection.communityconnection.config.mapping;

import com.communityconnection.communityconnection.jpa.domain.booking.BookingEntity;
import com.communityconnection.communityconnection.jpa.specification.booking.BookingFreeTimeFilterDTO;
import com.communityconnection.communityconnection.service.booking.dto.BookingCreateRequestDTO;
import com.communityconnection.communityconnection.service.booking.dto.BookingResponseDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BookingMapper {

    BookingResponseDTO toDTO(BookingEntity booking);

    BookingFreeTimeFilterDTO toDTO(BookingCreateRequestDTO bookingCreateRequestDTO);

}
