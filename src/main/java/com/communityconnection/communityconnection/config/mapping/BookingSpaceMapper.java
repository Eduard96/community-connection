package com.communityconnection.communityconnection.config.mapping;

import com.communityconnection.communityconnection.service.booking.dto.BookingSpaceCreateRequest;
import com.communityconnection.communityconnection.service.parking.dto.ParkingSpaceCreateRequestDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BookingSpaceMapper {

    BookingSpaceCreateRequest toDTO(ParkingSpaceCreateRequestDTO parkingSpaceCreateRequestDTO);

}
