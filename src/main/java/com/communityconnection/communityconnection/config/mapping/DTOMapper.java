package com.communityconnection.communityconnection.config.mapping;

import com.communityconnection.communityconnection.jpa.domain.user.UserEntity;
import com.communityconnection.communityconnection.service.user.dto.UserDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper(componentModel = "spring")
public interface DTOMapper {
    DTOMapper INSTANCE = Mappers.getMapper(DTOMapper.class);

    @Mapping(target = "createdBy", source = "createdBy")
    @Mapping(target = "lastModifiedBy", source = "lastModifiedBy")
    UserDTO toDTO(UserEntity user);

}
