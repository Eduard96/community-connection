package com.communityconnection.communityconnection.controller.account;

import com.communityconnection.communityconnection.service.security.SecurityService;
import com.communityconnection.communityconnection.service.user.account.AccountService;
import com.communityconnection.communityconnection.service.user.account.dto.AuthRequestDTO;
import com.communityconnection.communityconnection.service.user.account.dto.AuthResponseDTO;
import com.communityconnection.communityconnection.service.user.account.dto.ChangePasswordDTO;
import com.communityconnection.communityconnection.service.user.dto.CreatePasswordUserDTO;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Email;

import static org.springframework.http.HttpHeaders.AUTHORIZATION;

@Tag(name = "account")
@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
@Validated
public class AccountController {

    private final SecurityService securityService;
    private final AccountService accountService;

    @PostMapping("/create-password")
    public void createPassword(@RequestBody @Valid CreatePasswordUserDTO createPasswordUserDTO) {
        accountService.updatePassword(createPasswordUserDTO);
    }

    @PostMapping("/login")
    public ResponseEntity<AuthResponseDTO> login(@RequestBody @Valid AuthRequestDTO authRequestDTO) {
        AuthResponseDTO authResponseDTO = securityService.authenticate(authRequestDTO);
        return ResponseEntity.ok(authResponseDTO);
    }


    @PostMapping("/reset-password")
    public void resetPasswordRequest(@RequestParam @Email String email) {
        accountService.sendResetPasswordEmail(email);
    }

    /**
     * When user forgot his password
     * Request from reset-password page
     */
    @ApiResponse(description = "User forgot his password. Should be used after reset-password")
    @PostMapping("/change-password")
    public void changePassword(@RequestBody CreatePasswordUserDTO createPasswordUserDTO,
                               @RequestHeader(name = AUTHORIZATION) String token) {
        if (securityService.jwtHasReset(token))
            accountService.updatePassword(createPasswordUserDTO);
    }

    /**
     * When user change his password
     */
    @ApiResponse(description = "When logged in user change his password")
    @PutMapping("/change-password")
    public ResponseEntity<AuthResponseDTO> changePassword(@RequestBody @Valid ChangePasswordDTO changePasswordDTO) {
        securityService.authenticateForChangePassword(securityService.getUsername(), changePasswordDTO.getOldPassword());
        accountService.updatePassword(changePasswordDTO);
        AuthResponseDTO authResponseDTO = securityService.getAuthResponseDTO();
        return ResponseEntity.ok(authResponseDTO);
    }

    @PostMapping("/renew")
    public ResponseEntity<AuthResponseDTO> renew(@RequestHeader(name = AUTHORIZATION) String refreshToken) {
        AuthResponseDTO authResponseDTO = securityService.renew(refreshToken);
        return ResponseEntity.ok(authResponseDTO);
    }
}
