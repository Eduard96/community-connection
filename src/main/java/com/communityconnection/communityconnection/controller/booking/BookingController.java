package com.communityconnection.communityconnection.controller.booking;

import com.communityconnection.communityconnection.service.booking.BookingService;
import com.communityconnection.communityconnection.service.booking.dto.BookingCreateRequestDTO;
import com.communityconnection.communityconnection.service.booking.dto.BookingResponseDTO;
import com.communityconnection.communityconnection.utils.pageandsort.CustomPageRequest;
import com.communityconnection.communityconnection.utils.pageandsort.ImplSort;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Tag(name = "booking")
@RestController
@RequestMapping("/bookings")
@RequiredArgsConstructor
@Validated
public class BookingController {

    private final BookingService bookingService;

    @GetMapping
    public ResponseEntity<Page<BookingResponseDTO>> getAll(
            @RequestParam int page,
            @RequestParam int size,
            @RequestParam Sort.Direction dir) {
        PageRequest pageRequest = CustomPageRequest.from(page, size, ImplSort.by("id", dir));
        Page<BookingResponseDTO> bookings = bookingService.getAll(pageRequest);
        return ResponseEntity.ok(bookings);
    }

    @PostMapping
    public ResponseEntity<BookingResponseDTO> create(@RequestBody @Valid BookingCreateRequestDTO bookingCreateRequest) {
        BookingResponseDTO booking = bookingService.create(bookingCreateRequest);
        return ResponseEntity.ok(booking);
    }

    @DeleteMapping("/{id}")
    public void cancel(@PathVariable Long id) {
        bookingService.cancel(id);
    }

}
