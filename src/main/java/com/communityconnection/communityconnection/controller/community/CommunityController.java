package com.communityconnection.communityconnection.controller.community;

import com.communityconnection.communityconnection.service.community.CommunityService;
import com.communityconnection.communityconnection.service.community.dto.CommunityResponseDTO;
import com.communityconnection.communityconnection.utils.pageandsort.CustomPageRequest;
import com.communityconnection.communityconnection.utils.pageandsort.ImplSort;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;

@Tag(name = "community")
@RestController
@RequestMapping("/communities")
@RequiredArgsConstructor
@Validated
public class CommunityController {

    private final CommunityService communityService;

    @GetMapping
    public ResponseEntity<Page<CommunityResponseDTO>> getAll(
            @RequestParam int size,
            @RequestParam int page,
            @RequestParam Sort.Direction dir) {
        PageRequest pageRequest = CustomPageRequest.from(page, size, ImplSort.by("id", dir));
        Page<CommunityResponseDTO> communities = communityService.getAll(page, size, pageRequest);
        return ResponseEntity.ok(communities);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CommunityResponseDTO> getById(@PathVariable @Min(1) Long id) {
        CommunityResponseDTO community = communityService.getById(id);
        return ResponseEntity.ok(community);
    }

}
