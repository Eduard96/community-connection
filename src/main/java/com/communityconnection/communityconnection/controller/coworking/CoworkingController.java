package com.communityconnection.communityconnection.controller.coworking;

import com.communityconnection.communityconnection.service.coworking.CoworkingService;
import com.communityconnection.communityconnection.service.coworking.dto.CoworkingResponseDTO;
import com.communityconnection.communityconnection.utils.pageandsort.CustomPageRequest;
import com.communityconnection.communityconnection.utils.pageandsort.ImplSort;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;

@Tag(name = "coworking")
@RestController
@RequestMapping("/coworkings")
@RequiredArgsConstructor
@Validated
public class CoworkingController {

    private final CoworkingService coworkingService;

    @GetMapping
    public ResponseEntity<Page<CoworkingResponseDTO>> getAll(
            @RequestParam int size,
            @RequestParam int page,
            @RequestParam Sort.Direction dir) {
        PageRequest pageRequest = CustomPageRequest.from(page, size, ImplSort.by("id", dir));
        Page<CoworkingResponseDTO> communities = coworkingService.getAll(page, size, pageRequest);
        return ResponseEntity.ok(communities);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CoworkingResponseDTO> getById(@PathVariable @Min(1) Long id) {
        CoworkingResponseDTO coworking = coworkingService.getById(id);
        return ResponseEntity.ok(coworking);
    }

}
