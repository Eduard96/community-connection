package com.communityconnection.communityconnection.controller.parking;

import com.communityconnection.communityconnection.service.parking.ParkingService;
import com.communityconnection.communityconnection.service.parking.dto.ParkingResponseDTO;
import com.communityconnection.communityconnection.service.parking.dto.ParkingSpaceCreateRequestDTO;
import com.communityconnection.communityconnection.utils.pageandsort.CustomPageRequest;
import com.communityconnection.communityconnection.utils.pageandsort.ImplSort;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;

@Tag(name = "parking")
@RestController
@RequestMapping("/parkings")
@RequiredArgsConstructor
@Validated
public class ParkingController {

    private final ParkingService parkingService;

    @GetMapping
    public ResponseEntity<Page<ParkingResponseDTO>> getAll(
            @RequestParam int size,
            @RequestParam int page,
            @RequestParam Sort.Direction dir) {
        PageRequest pageRequest = CustomPageRequest.from(page, size, ImplSort.by("id", dir));
        Page<ParkingResponseDTO> communities = parkingService.getAll(page, size, pageRequest);
        return ResponseEntity.ok(communities);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ParkingResponseDTO> getAll(@PathVariable @Min(1) Long id) {
        ParkingResponseDTO parking = parkingService.getById(id);
        return ResponseEntity.ok(parking);
    }

    //For Space providers
    @PostMapping
    public void create(ParkingSpaceCreateRequestDTO parkingSpaceCreateRequestDTO) {
        parkingService.create(parkingSpaceCreateRequestDTO);
    }

}
