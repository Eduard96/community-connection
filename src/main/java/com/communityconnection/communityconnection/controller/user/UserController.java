package com.communityconnection.communityconnection.controller.user;

import com.communityconnection.communityconnection.service.user.UserService;
import com.communityconnection.communityconnection.service.user.dto.CreateUserDTO;
import com.communityconnection.communityconnection.service.user.dto.UserDTO;
import com.communityconnection.communityconnection.utils.pageandsort.ImplSort;
import com.communityconnection.communityconnection.utils.pageandsort.enums.UserSortField;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;


@Tag(name = "users")
@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
@Validated
public class UserController {

    private final UserService userService;

    @PostMapping
    public void create(@RequestBody @Valid CreateUserDTO createUserDTO) {
        userService.create(createUserDTO);
    }

    @PostMapping("/filter")
    public ResponseEntity<Page<UserDTO>> getAll(
            @RequestParam(name = "page", defaultValue = "1") @Min(value = 1, message = "Page index must not be less than one") Integer page,
            @RequestParam(name = "size", defaultValue = "20") @Min(value = 1) Integer size,
            @RequestParam(name = "sort", defaultValue = "ID") UserSortField sort,
            @RequestParam(name = "direction", defaultValue = "DESC") Sort.Direction direction) {
        PageRequest pageRequest = PageRequest.of(page - 1, size, ImplSort.by(sort.getName(), direction));
        return ResponseEntity.ok(userService.getAll(pageRequest));
    }

    @DeleteMapping
    public void delete(@RequestParam List<Long> ids) {
        userService.delete(ids);
    }
}
