package com.communityconnection.communityconnection.event;

import com.communityconnection.communityconnection.event.booking.events.ParkingSpaceBookingEvent;
import com.communityconnection.communityconnection.service.booking.dto.BookingCreateRequestDTO;
import com.communityconnection.communityconnection.utils.booking.BookingSpaceType;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CustomEventPublisher {

    private final ApplicationEventPublisher publisher;

    public void publishParkingSpaceBookingEvent(BookingCreateRequestDTO bookingCreateRequestDTO) {
        if (BookingSpaceType.PARKING_SPACE.equals(bookingCreateRequestDTO.getBookingSpaceType())) {
            final ParkingSpaceBookingEvent parkingSpaceBookingEvent = new ParkingSpaceBookingEvent(this, bookingCreateRequestDTO);
            publisher.publishEvent(parkingSpaceBookingEvent);
        }
    }

}
