package com.communityconnection.communityconnection.event.booking;

import com.communityconnection.communityconnection.event.booking.events.ParkingSpaceBookingEvent;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionalEventListener;

@Component
@RequiredArgsConstructor
public class BookingEventListener {

    @TransactionalEventListener
    @Async
    @Transactional(readOnly = true)
    public void handleParkingSpaceBookingEvent(ParkingSpaceBookingEvent event) {
        //TODO send email
    }

    //the same for the other spaces (but count of event types will depend on messages

}
