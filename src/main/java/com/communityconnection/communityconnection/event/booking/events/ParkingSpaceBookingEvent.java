package com.communityconnection.communityconnection.event.booking.events;

import com.communityconnection.communityconnection.service.booking.dto.BookingCreateRequestDTO;
import lombok.Getter;
import lombok.Setter;
import org.springframework.context.ApplicationEvent;

@Getter
@Setter
public class ParkingSpaceBookingEvent extends ApplicationEvent {

    private BookingCreateRequestDTO bookingCreateRequestDTO;

    public ParkingSpaceBookingEvent(Object source, BookingCreateRequestDTO bookingCreateRequestDTO) {
        super(source);
        this.bookingCreateRequestDTO = bookingCreateRequestDTO;
    }

}
