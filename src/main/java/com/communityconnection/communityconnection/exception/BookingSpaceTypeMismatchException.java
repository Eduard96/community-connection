package com.communityconnection.communityconnection.exception;

public class BookingSpaceTypeMismatchException extends RuntimeException {
    public BookingSpaceTypeMismatchException(String msg) {
        super(msg);
    }
}
