package com.communityconnection.communityconnection.exception;

public class BookingSpaceUnavailableException extends RuntimeException {

    public BookingSpaceUnavailableException(String msg) {
        super(msg);
    }

}
