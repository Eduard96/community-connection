package com.communityconnection.communityconnection.exception;

public class CommonValidationException extends RuntimeException {

    public CommonValidationException(String msg) {
        super(msg);
    }

}
