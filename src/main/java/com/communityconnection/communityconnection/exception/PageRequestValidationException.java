package com.communityconnection.communityconnection.exception;

public class PageRequestValidationException extends RuntimeException {
    public PageRequestValidationException(String message) {
        super(message);
    }
}