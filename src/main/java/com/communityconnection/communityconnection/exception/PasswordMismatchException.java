package com.communityconnection.communityconnection.exception;

public class PasswordMismatchException extends RuntimeException {

    public PasswordMismatchException(String password_mismatch) {
        super(password_mismatch);
    }
}
