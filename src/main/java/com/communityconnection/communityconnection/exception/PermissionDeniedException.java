package com.communityconnection.communityconnection.exception;

public class PermissionDeniedException extends RuntimeException {

    public PermissionDeniedException() {
        super("Permission denied exception");
    }
}
