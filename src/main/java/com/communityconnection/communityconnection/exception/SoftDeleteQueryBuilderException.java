package com.communityconnection.communityconnection.exception;

public class SoftDeleteQueryBuilderException extends RuntimeException {

    public SoftDeleteQueryBuilderException(String somethingWentWrong) {
        super(somethingWentWrong);
    }
}
