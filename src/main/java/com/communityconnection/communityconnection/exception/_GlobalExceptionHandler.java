package com.communityconnection.communityconnection.exception;

import com.communityconnection.communityconnection.exception.nonunique.AlreadyTakenException;
import com.communityconnection.communityconnection.exception.resourcenotfound.ResourceNotFoundException;
import com.communityconnection.communityconnection.utils.error.ApiError;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;
import io.jsonwebtoken.UnsupportedJwtException;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestClientException;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.util.NestedServletException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Objects.isNull;

@ControllerAdvice
public class _GlobalExceptionHandler {

    private static final Logger LOGGER = LogManager.getLogger(_GlobalExceptionHandler.class);

    private static final String SOMETHING_WENT_WRONG = "Something went wrong";

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public ResponseEntity<ApiError> handleResourceNotFoundException(HttpServletRequest req, ResourceNotFoundException e) {
        logError(req, e);
        return buildResponse(HttpStatus.NOT_FOUND, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(AlreadyTakenException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> handleNonUniqueResultException(HttpServletRequest req, AlreadyTakenException e) {
        logError(req, e);
        return buildResponse(HttpStatus.BAD_REQUEST, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> handleMethodArgumentNotValidException(HttpServletRequest req, MethodArgumentNotValidException e) {
        logError(req, e);
        Map<String, String> errors = e.getFieldErrors()
                .stream()
                .collect(Collectors.toMap(
                        FieldError::getField,
                        fieldError -> isNull(fieldError.getDefaultMessage()) ? "wrong value type" : fieldError.getDefaultMessage()));

        return ResponseEntity.badRequest().body(new ApiError(HttpStatus.BAD_REQUEST.value(), req.getRequestURI(), errors));
    }

    @ExceptionHandler(SignatureException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiError> handleSignatureException(HttpServletRequest req, SignatureException e) {
        logError(req, e);
        return buildResponse(HttpStatus.UNAUTHORIZED, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(ExpiredJwtException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiError> handleExpiredJwtException(HttpServletRequest req, ExpiredJwtException e) {
        logError(req, e);
        return buildResponse(HttpStatus.UNAUTHORIZED, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(MalformedJwtException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiError> handleMalformedJwtException(HttpServletRequest req, MalformedJwtException e) {
        logError(req, e);
        return buildResponse(HttpStatus.UNAUTHORIZED, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(UnsupportedJwtException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiError> handleUnsupportedJwtException(HttpServletRequest req, UnsupportedJwtException e) {
        logError(req, e);
        return buildResponse(HttpStatus.UNAUTHORIZED, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(BadCredentialsException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiError> handleBadCredentialsException(HttpServletRequest req, BadCredentialsException e) {
        logError(req, e);
        return buildResponse(HttpStatus.UNAUTHORIZED, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> handleConstraintViolationException(HttpServletRequest req, ConstraintViolationException e) {
        logError(req, e);
        return buildResponse(HttpStatus.BAD_REQUEST, prettifyMessage(e), req.getRequestURI());
    }

    @ExceptionHandler(PasswordMismatchException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> handlePasswordMismatchException(HttpServletRequest req, PasswordMismatchException e) {
        logError(req, e);
        return buildResponse(HttpStatus.BAD_REQUEST, prettifyMessage(e), req.getRequestURI());
    }

    @ExceptionHandler(RefreshTokenNotFoundException.class)
    @ResponseStatus(code = HttpStatus.FORBIDDEN)
    public ResponseEntity<ApiError> handleRefreshTokenNotFoundException(HttpServletRequest req, RefreshTokenNotFoundException e) {
        logError(req, e);
        return buildResponse(HttpStatus.FORBIDDEN, prettifyMessage(e), req.getRequestURI());
    }

    @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(code = HttpStatus.NOT_FOUND)
    public ResponseEntity<ApiError> handleHttpRequestMethodNotSupportedException(HttpServletRequest req, HttpRequestMethodNotSupportedException e) {
        logError(req, e);
        return buildResponse(HttpStatus.NOT_FOUND, prettifyMessage(e), req.getRequestURI());
    }

    @ExceptionHandler(InvalidFormatException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> handleInvalidFormatException(HttpServletRequest req, InvalidFormatException e) {
        logError(req, e);
        return buildResponse(HttpStatus.BAD_REQUEST, SOMETHING_WENT_WRONG, req.getRequestURI());
    }

    @ExceptionHandler(NestedServletException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ApiError> handleNestedServletException(HttpServletRequest req, NestedServletException e) {
        logError(req, e);
        return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, SOMETHING_WENT_WRONG, req.getRequestURI());
    }

    @ExceptionHandler(NullPointerException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ApiError> handleNullPointerException(HttpServletRequest req, NullPointerException e) {
        logError(req, e);
        return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, SOMETHING_WENT_WRONG, req.getRequestURI());
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> handleMethodArgumentTypeMismatchException(HttpServletRequest req, MethodArgumentTypeMismatchException e) {
        logError(req, e);
        return buildResponse(HttpStatus.BAD_REQUEST, SOMETHING_WENT_WRONG, req.getRequestURI());
    }

    @ExceptionHandler(IllegalArgumentException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> handleIllegalArgumentException(HttpServletRequest req, IllegalArgumentException e) {
        logError(req, e);
        return buildResponse(HttpStatus.BAD_REQUEST, SOMETHING_WENT_WRONG, req.getRequestURI());
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> handleHttpMessageNotReadableException(HttpServletRequest req, HttpMessageNotReadableException e) {
        logError(req, e);
        return buildResponse(HttpStatus.BAD_REQUEST, "Required request BODY! or wrong type in JSON", req.getRequestURI());
    }

    @ExceptionHandler(AuthenticationCredentialsNotFoundException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiError> handleAuthenticationCredentialsNotFoundException(HttpServletRequest req, AuthenticationCredentialsNotFoundException e) {
        logError(req, e);
        return buildResponse(HttpStatus.UNAUTHORIZED, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(AuthenticationException.class)
    @ResponseStatus(code = HttpStatus.UNAUTHORIZED)
    public ResponseEntity<ApiError> handleAuthenticationException(HttpServletRequest req, AuthenticationException e) {
        logError(req, e);
        return buildResponse(HttpStatus.UNAUTHORIZED, "Invalid login or password", req.getRequestURI());
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ResponseEntity<ApiError> handleMissingServletRequestParameterException(HttpServletRequest req, MissingServletRequestParameterException e) {
        logError(req, e);
        return buildResponse(HttpStatus.BAD_REQUEST, e.getMessage() + " or used symbols that not allowed", req.getRequestURI());
    }

    @ExceptionHandler(MultipartException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    public ResponseEntity<ApiError> handleMultipartException(HttpServletRequest req, MultipartException e) {
        logError(req, e);
        return buildResponse(HttpStatus.UNPROCESSABLE_ENTITY, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(SoftDeleteQueryBuilderException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ApiError> handleSoftDeleteQueryBuilderException(HttpServletRequest req, SoftDeleteQueryBuilderException e) {
        logError(req, e);
        return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), req.getRequestURI());
    }


    @ExceptionHandler(HttpClientErrorException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)
    public ResponseEntity<ApiError> handleHttpClientErrorException(HttpServletRequest req, HttpClientErrorException e) {
        logError(req, e);
        return buildResponse(HttpStatus.UNPROCESSABLE_ENTITY, SOMETHING_WENT_WRONG, req.getRequestURI());
    }

    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ApiError> handleSQLIntegrityConstraintViolationException(HttpServletRequest req, SQLIntegrityConstraintViolationException e) {
        logError(req, e);
        return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage(), req.getRequestURI());
    }

    @ExceptionHandler(RestClientException.class)
    @ResponseStatus(code = HttpStatus.INTERNAL_SERVER_ERROR)
    public ResponseEntity<ApiError> handleRestClientException(HttpServletRequest req, RestClientException e) {
        logError(req, e);
        return buildResponse(HttpStatus.INTERNAL_SERVER_ERROR, SOMETHING_WENT_WRONG, req.getRequestURI());
    }

    private String prettifyMessage(Exception e) {
        return e.getMessage().substring(e.getMessage().indexOf('.') + 1);
    }

    private ResponseEntity<ApiError> buildResponse(HttpStatus httpCode, String message, String requestURI) {
        Map<String, String> errors = new HashMap<>();
        errors.put("message", message);
        ApiError apiError = new ApiError(httpCode.value(), requestURI, errors);
        return ResponseEntity.status(httpCode).body(apiError);

    }

    private void logError(HttpServletRequest req, Exception e) {
        LOGGER.warn(e.getMessage());
        LOGGER.warn("RequestURI {}", req.getRequestURI());
        LOGGER.error(ExceptionUtils.getStackTrace(e));
    }

}
