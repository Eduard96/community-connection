package com.communityconnection.communityconnection.exception.nonunique;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AlreadyTakenException extends RuntimeException {

    private static final Logger LOGGER = LogManager.getLogger(AlreadyTakenException.class);
    private static final String ALREADY_TAKEN_MESSAGE = "%s '%s' already taken";

    public AlreadyTakenException(String fieldName, String value) {
        this(buildLogMessage(fieldName, value));
    }

    public AlreadyTakenException(String message) {
        super(logReturn(message));
    }

    private static String buildLogMessage(String fieldName, String value) {
        String message = String.format(ALREADY_TAKEN_MESSAGE, fieldName, value);
        return logReturn(message);
    }

    private static String logReturn(String message) {
        LOGGER.warn(message);
        return message;
    }
}
