package com.communityconnection.communityconnection.jpa.domain;

import com.communityconnection.communityconnection.service.user.details.UserDetailsImpl;
import lombok.Getter;
import lombok.Setter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import javax.persistence.*;
import java.time.LocalDateTime;

import static java.util.Objects.nonNull;

@Getter
@Setter
@MappedSuperclass
public abstract class _BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private LocalDateTime createdDate;

    @Column(nullable = false)
    private LocalDateTime lastModifiedDate;

    private boolean deleted;

    @Column
    private Long createdBy;

    @Column
    private Long lastModifiedBy;

    @PrePersist
    public void prePersist() {
        if (nonNull(this.createdDate) && nonNull(lastModifiedDate)) {
            return;
        }
        LocalDateTime now = LocalDateTime.now();
        setCreatedDate(now);
        setLastModifiedDate(now);
        setCreatedBy();
    }

    @PreUpdate
    public void preUpdate() {
        setLastModifiedDate(LocalDateTime.now());
        setLastModifiedBy();
    }

    public void setCreatedBy() {
        if (nonNull(getAuthentication())) {
            UserDetailsImpl userDetails = (UserDetailsImpl) getAuthentication().getPrincipal();
            setCreatedBy(userDetails.getId());
            setLastModifiedBy(userDetails.getId());
        }
    }

    public void setLastModifiedBy() {
        if (nonNull(getAuthentication())) {
            UserDetailsImpl userDetails = (UserDetailsImpl) getAuthentication().getPrincipal();
            setLastModifiedBy(userDetails.getId());
        }
    }

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }
}