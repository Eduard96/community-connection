package com.communityconnection.communityconnection.jpa.domain.booking;

import com.communityconnection.communityconnection.jpa.domain._BaseEntity;
import com.communityconnection.communityconnection.jpa.domain.bookspace.BookingSpaceEntity;
import com.communityconnection.communityconnection.jpa.domain.user.UserEntity;
import com.communityconnection.communityconnection.utils.EntityName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = EntityName.BOOKING)
public class BookingEntity extends _BaseEntity {

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private UserEntity user;

    @ManyToOne(optional = false, fetch = FetchType.LAZY)
    private BookingSpaceEntity bookingSpace;

    @Column(nullable = false)
    private LocalDateTime startTime;

    @Column(nullable = false)
    private LocalDateTime endTime;

}
