package com.communityconnection.communityconnection.jpa.domain.bookspace;

import com.communityconnection.communityconnection.jpa.domain._BaseEntity;
import com.communityconnection.communityconnection.utils.EntityName;
import com.communityconnection.communityconnection.utils.booking.BookingSpaceType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

/**
 * Every Booking space will be associated with one of booking spaces
 **/
@Getter
@Setter
@Entity
@Table(name = EntityName.BOOKING_SPACE)
public class BookingSpaceEntity extends _BaseEntity {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private BookingSpaceType bookingSpaceType;

    @OneToOne(mappedBy = "bookingSpace")
    private ParkingSpaceEntity parkingSpace;

    @OneToOne(mappedBy = "bookingSpace")
    private CoworkingRoomSpaceEntity coworkingRoomSpace;

}
