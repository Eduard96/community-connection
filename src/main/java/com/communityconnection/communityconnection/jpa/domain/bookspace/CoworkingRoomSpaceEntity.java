package com.communityconnection.communityconnection.jpa.domain.bookspace;

import com.communityconnection.communityconnection.exception.BookingSpaceTypeMismatchException;
import com.communityconnection.communityconnection.jpa.domain._BaseEntity;
import com.communityconnection.communityconnection.utils.EntityName;
import com.communityconnection.communityconnection.utils.booking.BookingSpaceType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = EntityName.COWORKING_SPACE)
public class CoworkingRoomSpaceEntity extends _BaseEntity {

    @Column(nullable = false)
    private int tableNumber;

    @Column(nullable = false)
    private int meetingRoomFloor;

    @Column(nullable = false)
    private String peopleCount;

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(unique = true)
    private BookingSpaceEntity bookingSpace;

    public void setBookingSpace(BookingSpaceEntity bookingSpace) {
        boolean isCoworkingSpace = BookingSpaceType.COWORKING_SPACE.equals(bookingSpace.getBookingSpaceType());
        if (!isCoworkingSpace) {
            throw new BookingSpaceTypeMismatchException("Booking space type mismatch");
        }
        this.bookingSpace = bookingSpace;
    }
}
