package com.communityconnection.communityconnection.jpa.domain.bookspace;

import com.communityconnection.communityconnection.exception.BookingSpaceTypeMismatchException;
import com.communityconnection.communityconnection.jpa.domain._BaseEntity;
import com.communityconnection.communityconnection.utils.EntityName;
import com.communityconnection.communityconnection.utils.booking.BookingSpaceType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = EntityName.PARKING_SPACE)
public class ParkingSpaceEntity extends _BaseEntity {

    @Column(nullable = false)
    private int parkingSpaceFloor;

    @Column(nullable = false)
    private int parkingSpaceNumber;

    @Column(nullable = false)
    private boolean hasEVCharging;

    @OneToOne(optional = false, fetch = FetchType.LAZY)
    @JoinColumn(unique = true)
    private BookingSpaceEntity bookingSpace;

    public void setBookingSpace(BookingSpaceEntity bookingSpace) {
        boolean isParkingSpace = BookingSpaceType.PARKING_SPACE.equals(bookingSpace.getBookingSpaceType());
        if (!isParkingSpace) {
            throw new BookingSpaceTypeMismatchException("Booking space type mismatch");
        }
        this.bookingSpace = bookingSpace;
    }
}
