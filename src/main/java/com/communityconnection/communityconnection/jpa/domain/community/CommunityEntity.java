package com.communityconnection.communityconnection.jpa.domain.community;


import com.communityconnection.communityconnection.jpa.domain._BaseEntity;
import com.communityconnection.communityconnection.utils.EntityName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = EntityName.COMMUNITY_USER)
public class CommunityEntity extends _BaseEntity {

    @Column(nullable = false)
    private String name;

}
