package com.communityconnection.communityconnection.jpa.domain.community;

import com.communityconnection.communityconnection.jpa.domain.user.UserEntity;
import com.communityconnection.communityconnection.utils.EntityName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(
        name = EntityName.COMMUNITY_USER,
        uniqueConstraints = @UniqueConstraint(name = "UK_COMMUNITY_USER", columnNames = {"community_id", "user_id"})
)
public class CommunityUserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(optional = false, fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private CommunityEntity community;

    @ManyToOne(optional = false, fetch = FetchType.LAZY,
            cascade = {CascadeType.PERSIST, CascadeType.REFRESH})
    private UserEntity user;
}
