package com.communityconnection.communityconnection.jpa.domain.token;

import com.communityconnection.communityconnection.jpa.domain._BaseEntity;
import com.communityconnection.communityconnection.utils.EntityName;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = EntityName.REFRESH_TOKEN)
public class RefreshTokenEntity extends _BaseEntity {

    @Column(nullable = false)
    private String refreshToken;
}
