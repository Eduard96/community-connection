package com.communityconnection.communityconnection.jpa.repository.booking;

import com.communityconnection.communityconnection.jpa.domain.booking.BookingEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends JpaRepository<BookingEntity, Long>,
        JpaSpecificationExecutor<BookingEntity> {

    @EntityGraph(attributePaths = "bookingSpace")
    Page<BookingEntity> findAllByUserId(Long userId, PageRequest pageRequest);
}
