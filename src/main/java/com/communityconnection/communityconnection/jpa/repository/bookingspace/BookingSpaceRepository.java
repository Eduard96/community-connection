package com.communityconnection.communityconnection.jpa.repository.bookingspace;

import com.communityconnection.communityconnection.jpa.domain.bookspace.BookingSpaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingSpaceRepository extends JpaRepository<BookingSpaceEntity, Long> {
}
