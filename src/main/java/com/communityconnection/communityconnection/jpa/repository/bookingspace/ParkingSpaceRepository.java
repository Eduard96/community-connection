package com.communityconnection.communityconnection.jpa.repository.bookingspace;

import com.communityconnection.communityconnection.jpa.domain.bookspace.ParkingSpaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ParkingSpaceRepository extends JpaRepository<ParkingSpaceEntity, Long> {
}
