package com.communityconnection.communityconnection.jpa.repository.community;

import com.communityconnection.communityconnection.jpa.domain.community.CommunityEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommunityRepository extends JpaRepository<CommunityEntity, Long> {
}
