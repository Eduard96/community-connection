package com.communityconnection.communityconnection.jpa.repository.coworking;

import com.communityconnection.communityconnection.jpa.domain.bookspace.CoworkingRoomSpaceEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CoworkingRepository extends JpaRepository<CoworkingRoomSpaceEntity, Long> {
}
