package com.communityconnection.communityconnection.jpa.specification.booking;

import com.communityconnection.communityconnection.utils.booking.BookingSpaceType;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BookingFreeTimeFilterDTO {

    private Long bookingSpaceId;
    private BookingSpaceType bookingSpaceType;
    private LocalDateTime startTime;
    private LocalDateTime endTime;

}
