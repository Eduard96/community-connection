package com.communityconnection.communityconnection.jpa.specification.booking;

import com.communityconnection.communityconnection.jpa.domain.booking.BookingEntity;
import com.communityconnection.communityconnection.jpa.domain.booking.BookingEntity_;
import com.communityconnection.communityconnection.jpa.domain.bookspace.BookingSpaceEntity;
import com.communityconnection.communityconnection.jpa.domain.bookspace.BookingSpaceEntity_;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

public class BookingSpecification {

    private final BookingFreeTimeFilterDTO freeTimeFilter;

    public BookingSpecification(BookingFreeTimeFilterDTO freeTimeFilter) {
        this.freeTimeFilter = freeTimeFilter;
    }

    public Specification<BookingEntity> findFreeBookingSpace() {
        return Specification.where((root, criteriaQuery, criteriaBuilder) -> {
            Join<BookingEntity, BookingSpaceEntity> bookingSpaceJoin = root
                    .join(BookingEntity_.bookingSpace, JoinType.LEFT);
            List<Predicate> predicates = new ArrayList<>();

            Predicate bookingSpacePredicate = criteriaBuilder
                    .equal(
                            bookingSpaceJoin.get(BookingSpaceEntity_.ID),
                            freeTimeFilter.getBookingSpaceId()
                    );
            predicates.add(bookingSpacePredicate);


            Predicate bookingSpaceTypePredicate = criteriaBuilder
                    .equal(
                            bookingSpaceJoin.get(BookingSpaceEntity_.BOOKING_SPACE_TYPE),
                            freeTimeFilter.getBookingSpaceType()
                    );
            predicates.add(bookingSpaceTypePredicate);


            Predicate overlappingStartTime = criteriaBuilder
                    .lessThanOrEqualTo(root.get(BookingEntity_.START_TIME), freeTimeFilter.getEndTime());
            Predicate overlappingEndTime = criteriaBuilder
                    .greaterThanOrEqualTo(root.get(BookingEntity_.END_TIME), freeTimeFilter.getStartTime());
            Predicate overlappingPredicates = criteriaBuilder
                    .and(bookingSpacePredicate, overlappingStartTime, overlappingEndTime);
            predicates.add(criteriaBuilder.not(overlappingPredicates));

            criteriaQuery.distinct(true);
            return criteriaBuilder.and(predicates.toArray(new Predicate[0]));
        });
    }

}
