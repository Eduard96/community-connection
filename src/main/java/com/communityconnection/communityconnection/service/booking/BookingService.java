package com.communityconnection.communityconnection.service.booking;

import com.communityconnection.communityconnection.service.booking.dto.BookingCreateRequestDTO;
import com.communityconnection.communityconnection.service.booking.dto.BookingResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface BookingService {
    Page<BookingResponseDTO> getAll(PageRequest pageRequest);

    BookingResponseDTO create(BookingCreateRequestDTO bookingCreateRequest);

    void cancel(Long id);

}
