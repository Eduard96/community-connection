package com.communityconnection.communityconnection.service.booking;

import com.communityconnection.communityconnection.config.mapping.BookingMapper;
import com.communityconnection.communityconnection.event.CustomEventPublisher;
import com.communityconnection.communityconnection.exception.CommonValidationException;
import com.communityconnection.communityconnection.jpa.domain.booking.BookingEntity;
import com.communityconnection.communityconnection.jpa.domain.bookspace.BookingSpaceEntity;
import com.communityconnection.communityconnection.jpa.repository.booking.BookingRepository;
import com.communityconnection.communityconnection.service.booking.dto.BookingCreateRequestDTO;
import com.communityconnection.communityconnection.service.booking.dto.BookingResponseDTO;
import com.communityconnection.communityconnection.service.security.SecurityService;
import com.communityconnection.communityconnection.service.user.dto.UserDTO;
import com.communityconnection.communityconnection.service.utils.FindOne;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class BookingServiceImpl implements BookingService {

    private final BookingRepository bookingRepository;
    private final BookingSpaceService bookingSpaceService;

    private final CustomEventPublisher customEventPublisher;

    private final FindOne findOne;
    private final BookingMapper bookingMapper;
    private final SecurityService securityService;

    @Override
    @Transactional(readOnly = true)
    public Page<BookingResponseDTO> getAll(PageRequest pageRequest) {
        UserDTO user = securityService.getUserDTO();
        Page<BookingEntity> bookingEntities = bookingRepository.findAllByUserId(user.getId(), pageRequest);
        return bookingEntities.map(bookingMapper::toDTO);
    }

    @Override
    @Transactional
    public BookingResponseDTO create(BookingCreateRequestDTO bookingCreateRequest) {
        UserDTO user = securityService.getUserDTO();
        bookingSpaceService.validateFreeTime(bookingCreateRequest);

        BookingEntity booking = new BookingEntity();
        booking.setStartTime(bookingCreateRequest.getStartTime());
        booking.setEndTime(bookingCreateRequest.getEndTime());
        booking.setUser(findOne.getUser(user.getId()));

        BookingSpaceEntity bookingSpace = findOne.getBookingSpace(bookingCreateRequest.getBookingSpaceId());
        booking.setBookingSpace(bookingSpace);
        bookingRepository.save(booking);

        customEventPublisher.publishParkingSpaceBookingEvent(bookingCreateRequest);
        return bookingMapper.toDTO(booking);
    }

    @Override
    @Transactional
    public void cancel(Long id) {
        BookingEntity booking = findOne.getBooking(id);
        //If you need to save booking history
        //I would use second database, or table (without FKs)
        Duration duration = Duration.between(LocalDateTime.now(), booking.getStartTime());
        if (duration.getSeconds() > 60 * 15) {
            bookingRepository.delete(booking);
        }
        throw new CommonValidationException("You can't cancel booking 15 minutes before.");
    }
}
