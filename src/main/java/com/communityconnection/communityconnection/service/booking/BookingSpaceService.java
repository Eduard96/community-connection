package com.communityconnection.communityconnection.service.booking;

import com.communityconnection.communityconnection.jpa.domain.bookspace.BookingSpaceEntity;
import com.communityconnection.communityconnection.service.booking.dto.BookingCreateRequestDTO;
import com.communityconnection.communityconnection.service.booking.dto.BookingSpaceCreateRequest;

public interface BookingSpaceService {
    void validateFreeTime(BookingCreateRequestDTO bookingCreateRequest);

    BookingSpaceEntity create(BookingSpaceCreateRequest dto);
}
