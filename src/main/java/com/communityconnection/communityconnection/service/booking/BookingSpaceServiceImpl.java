package com.communityconnection.communityconnection.service.booking;

import com.communityconnection.communityconnection.config.mapping.BookingMapper;
import com.communityconnection.communityconnection.exception.BookingSpaceUnavailableException;
import com.communityconnection.communityconnection.jpa.domain.booking.BookingEntity;
import com.communityconnection.communityconnection.jpa.domain.bookspace.BookingSpaceEntity;
import com.communityconnection.communityconnection.jpa.repository.booking.BookingRepository;
import com.communityconnection.communityconnection.jpa.repository.bookingspace.BookingSpaceRepository;
import com.communityconnection.communityconnection.jpa.specification.booking.BookingFreeTimeFilterDTO;
import com.communityconnection.communityconnection.jpa.specification.booking.BookingSpecification;
import com.communityconnection.communityconnection.service.booking.dto.BookingCreateRequestDTO;
import com.communityconnection.communityconnection.service.booking.dto.BookingSpaceCreateRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
public class BookingSpaceServiceImpl implements BookingSpaceService {

    private final BookingRepository bookingRepository;
    private final BookingSpaceRepository bookingSpaceRepository;

    private final BookingMapper bookingMapper;

    @Override
    @Transactional(readOnly = true)
    public void validateFreeTime(BookingCreateRequestDTO bookingCreateRequest) {
        BookingFreeTimeFilterDTO freeTimeFilter = bookingMapper.toDTO(bookingCreateRequest);
        BookingSpecification bookingSpecification = new BookingSpecification(freeTimeFilter);
        List<BookingEntity> bookingEntities = bookingRepository.findAll(bookingSpecification.findFreeBookingSpace());
        if (bookingEntities.isEmpty()) {
            throw new BookingSpaceUnavailableException("Booking space unavailable");
        }
    }

    @Override
    @Transactional
    public BookingSpaceEntity create(BookingSpaceCreateRequest dto) {
        BookingSpaceEntity bookingSpace = new BookingSpaceEntity();
        bookingSpace.setName(dto.getName());
        bookingSpace.setAddress(dto.getAddress());
        bookingSpace.setBookingSpaceType(dto.getBookingSpaceType());
        return bookingSpaceRepository.save(bookingSpace);
    }
}
