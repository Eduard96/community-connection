package com.communityconnection.communityconnection.service.booking.dto;

import com.communityconnection.communityconnection.utils.booking.BookingSpaceType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class BookingCreateRequestDTO {

    @NotNull
    private Long bookingSpaceId;
    @NotNull
    private BookingSpaceType bookingSpaceType;
    @NotNull
    private LocalDateTime startTime;
    @NotNull
    private LocalDateTime endTime;

}
