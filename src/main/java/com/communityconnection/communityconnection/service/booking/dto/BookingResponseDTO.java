package com.communityconnection.communityconnection.service.booking.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class BookingResponseDTO {

    private BookingSpaceResponseDTO bookingSpace;

    private LocalDateTime startTime;

    private LocalDateTime endTime;

}
