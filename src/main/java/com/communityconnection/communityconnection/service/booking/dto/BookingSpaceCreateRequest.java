package com.communityconnection.communityconnection.service.booking.dto;

import com.communityconnection.communityconnection.utils.booking.BookingSpaceType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BookingSpaceCreateRequest {

    private String name;

    private String address;

    private BookingSpaceType bookingSpaceType;

}
