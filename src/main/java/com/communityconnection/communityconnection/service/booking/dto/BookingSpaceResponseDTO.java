package com.communityconnection.communityconnection.service.booking.dto;

import com.communityconnection.communityconnection.utils.booking.BookingSpaceType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class BookingSpaceResponseDTO {

    private String name;

    private String address;

    private BookingSpaceType bookingSpaceType;

}
