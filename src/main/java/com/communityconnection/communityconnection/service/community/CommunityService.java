package com.communityconnection.communityconnection.service.community;

import com.communityconnection.communityconnection.service.community.dto.CommunityResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface CommunityService {
    Page<CommunityResponseDTO> getAll(int page, int size, PageRequest pageRequest);

    CommunityResponseDTO getById(Long id);
}
