package com.communityconnection.communityconnection.service.community;

import com.communityconnection.communityconnection.jpa.repository.community.CommunityRepository;
import com.communityconnection.communityconnection.service.community.dto.CommunityResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CommunityServiceImpl implements CommunityService {

    private final CommunityRepository communityRepository;

    @Override
    @Transactional(readOnly = true)
    public Page<CommunityResponseDTO> getAll(int page, int size, PageRequest pageRequest) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public CommunityResponseDTO getById(Long id) {
        return null;
    }
}
