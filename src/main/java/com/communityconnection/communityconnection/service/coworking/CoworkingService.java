package com.communityconnection.communityconnection.service.coworking;

import com.communityconnection.communityconnection.service.coworking.dto.CoworkingResponseDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface CoworkingService {
    Page<CoworkingResponseDTO> getAll(int page, int size, PageRequest pageRequest);

    CoworkingResponseDTO getById(Long id);
}
