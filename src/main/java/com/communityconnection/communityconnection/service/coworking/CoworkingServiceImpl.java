package com.communityconnection.communityconnection.service.coworking;

import com.communityconnection.communityconnection.jpa.repository.bookingspace.CoworkingRoomSpaceRepository;
import com.communityconnection.communityconnection.service.coworking.dto.CoworkingResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class CoworkingServiceImpl implements CoworkingService {

    private final CoworkingRoomSpaceRepository coworkingRoomSpaceRepository;

    @Override
    @Transactional(readOnly = true)
    public Page<CoworkingResponseDTO> getAll(int page, int size, PageRequest pageRequest) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public CoworkingResponseDTO getById(Long id) {
        return null;
    }
}
