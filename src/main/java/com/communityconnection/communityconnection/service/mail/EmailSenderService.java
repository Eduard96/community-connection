package com.communityconnection.communityconnection.service.mail;


import com.communityconnection.communityconnection.service.user.dto.BaseUserDTO;

public interface EmailSenderService {
    void sendCreatePasswordEmail(BaseUserDTO createUserDTO, String token);

    void sendResetPasswordEmail(String email, String username, String password);
}
