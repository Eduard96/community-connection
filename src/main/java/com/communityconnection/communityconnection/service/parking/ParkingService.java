package com.communityconnection.communityconnection.service.parking;

import com.communityconnection.communityconnection.service.parking.dto.ParkingResponseDTO;
import com.communityconnection.communityconnection.service.parking.dto.ParkingSpaceCreateRequestDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

public interface ParkingService {

    Page<ParkingResponseDTO> getAll(int page, int size, PageRequest pageRequest);

    ParkingResponseDTO getById(Long id);

    void create(ParkingSpaceCreateRequestDTO parkingSpaceCreateRequestDTO);
}
