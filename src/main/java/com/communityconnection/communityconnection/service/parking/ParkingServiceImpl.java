package com.communityconnection.communityconnection.service.parking;

import com.communityconnection.communityconnection.config.mapping.BookingSpaceMapper;
import com.communityconnection.communityconnection.jpa.domain.bookspace.BookingSpaceEntity;
import com.communityconnection.communityconnection.jpa.domain.bookspace.ParkingSpaceEntity;
import com.communityconnection.communityconnection.jpa.repository.bookingspace.ParkingSpaceRepository;
import com.communityconnection.communityconnection.service.booking.BookingSpaceService;
import com.communityconnection.communityconnection.service.parking.dto.ParkingResponseDTO;
import com.communityconnection.communityconnection.service.parking.dto.ParkingSpaceCreateRequestDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class ParkingServiceImpl implements ParkingService {

    private final ParkingSpaceRepository parkingSpaceRepository;
    private final BookingSpaceService bookingSpaceService;

    private final BookingSpaceMapper bookingSpaceMapper;

    @Override
    @Transactional
    public void create(ParkingSpaceCreateRequestDTO parkingSpaceDTO) {
        BookingSpaceEntity bookingSpace = bookingSpaceService.create(bookingSpaceMapper.toDTO(parkingSpaceDTO));
        ParkingSpaceEntity parkingSpace = new ParkingSpaceEntity();
        parkingSpace.setBookingSpace(bookingSpace);
        parkingSpace.setParkingSpaceFloor(parkingSpaceDTO.getParkingSpaceFloor());
        parkingSpace.setParkingSpaceNumber(parkingSpaceDTO.getParkingSpaceNumber());
        parkingSpaceRepository.save(parkingSpace);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<ParkingResponseDTO> getAll(int page, int size, PageRequest pageRequest) {
        return null;
    }

    @Override
    @Transactional(readOnly = true)
    public ParkingResponseDTO getById(Long id) {
        return null;
    }
}
