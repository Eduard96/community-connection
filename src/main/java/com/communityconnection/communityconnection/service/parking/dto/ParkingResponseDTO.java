package com.communityconnection.communityconnection.service.parking.dto;

import com.communityconnection.communityconnection.service.booking.dto.BookingSpaceResponseDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ParkingResponseDTO {

    private int parkingSpaceFloor;

    private int parkingSpaceNumber;

    private boolean hasEVCharging;

    private BookingSpaceResponseDTO bookingSpace;

}
