package com.communityconnection.communityconnection.service.parking.dto;

import com.communityconnection.communityconnection.utils.booking.BookingSpaceType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;

@Getter
@Setter
public class ParkingSpaceCreateRequestDTO {

    @Column(nullable = false)
    private String name;

    @Column(nullable = false)
    private String address;

    @Column(nullable = false)
    private BookingSpaceType bookingSpaceType;

    @Column(nullable = false)
    private int parkingSpaceFloor;

    @Column(nullable = false)
    private int parkingSpaceNumber;

    @Column(nullable = false)
    private boolean hasEVCharging;

}
