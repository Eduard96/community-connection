package com.communityconnection.communityconnection.service.refreshtoken;

import com.communityconnection.communityconnection.jpa.domain.token.RefreshTokenEntity;
import com.communityconnection.communityconnection.jpa.repository.token.RefreshTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RefreshTokenService {

    private final RefreshTokenRepository refreshTokenRepository;

    @Autowired
    public RefreshTokenService(RefreshTokenRepository refreshTokenRepository) {
        this.refreshTokenRepository = refreshTokenRepository;
    }

    @Transactional
    public void save(String token) {
        RefreshTokenEntity refreshToken = new RefreshTokenEntity();
        refreshToken.setRefreshToken(token.substring(token.lastIndexOf(".")));
        refreshTokenRepository.save(refreshToken);
    }

    @Transactional
    public void delete(String token) {
        refreshTokenRepository.deleteByRefreshToken(token.substring(token.lastIndexOf(".")));
    }

    @Transactional(readOnly = true)
    public boolean exists(String token) {
        return refreshTokenRepository.existsByRefreshToken(token.substring(token.lastIndexOf(".")));
    }
}
