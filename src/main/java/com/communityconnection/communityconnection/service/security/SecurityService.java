package com.communityconnection.communityconnection.service.security;


import com.communityconnection.communityconnection.jpa.domain.user.UserEntity;
import com.communityconnection.communityconnection.service.user.account.dto.AuthRequestDTO;
import com.communityconnection.communityconnection.service.user.account.dto.AuthResponseDTO;
import com.communityconnection.communityconnection.service.user.dto.UserDTO;

public interface SecurityService {

    String generateTempToken(String username, String password);

    UserDTO getUserDTO(String username);

    void authenticateForChangePassword(String username, String oldPassword);

    AuthResponseDTO authenticate(AuthRequestDTO authRequestDTO);

    UserDTO getUserDTO();

    String getUsername();

    UserEntity getUserEntity();

    AuthResponseDTO getAuthResponseDTO();

    boolean jwtHasReset(String token);

    AuthResponseDTO renew(String refreshToken);

    boolean isOwner(Long createdById);

}
