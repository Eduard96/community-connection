package com.communityconnection.communityconnection.service.security;

import com.communityconnection.communityconnection.config.jwt.JwtTokenProvider;
import com.communityconnection.communityconnection.exception.RefreshTokenNotFoundException;
import com.communityconnection.communityconnection.jpa.domain.user.UserEntity;
import com.communityconnection.communityconnection.service.refreshtoken.RefreshTokenService;
import com.communityconnection.communityconnection.service.user.UserService;
import com.communityconnection.communityconnection.service.user.account.dto.AuthRequestDTO;
import com.communityconnection.communityconnection.service.user.account.dto.AuthResponseDTO;
import com.communityconnection.communityconnection.service.user.dto.UserDTO;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class SecurityServiceImpl implements SecurityService {

    private static final Logger LOGGER = LogManager.getLogger(SecurityServiceImpl.class);

    private final JwtTokenProvider jwtTokenProvider;
    private final AuthenticationManager authenticationManager;
    private final UserService userService;
    private final RefreshTokenService refreshTokenService;

    @Override
    public String generateTempToken(String username, String password) {
        LOGGER.info("Generate temp JWT token");
        return jwtTokenProvider.generateOneTimeToken(username, password);
    }

    @Override
    public void authenticateForChangePassword(String username, String oldPassword) {
        LOGGER.info("authentication by username: {}, and old password", username);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, oldPassword));
    }

    @Override
    @Transactional(readOnly = true)
    public AuthResponseDTO authenticate(AuthRequestDTO authRequestDTO) {
        String username = getUsername(authRequestDTO);
        LOGGER.info("Login authentication. Username: {}", username);
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                username,
                authRequestDTO.getPassword()));
        return createAuthorizationResponse(authRequestDTO);
    }

    private String getUsername(AuthRequestDTO authRequestDTO) {
        if (userService.existsByUsername(authRequestDTO.getLogin())) {
            return authRequestDTO.getLogin();
        }
        return userService.findByEmail(authRequestDTO.getLogin());
    }

    private AuthResponseDTO createAuthorizationResponse(AuthRequestDTO authRequestDTO) {
        LOGGER.info("Authorization response creation");
        String username = getUsername(authRequestDTO);
        UserDTO userDTO = getUserDTO(username);
        AuthResponseDTO authResponseModel = new AuthResponseDTO(userDTO);

        String password = getPasswordByUsername(userDTO.getUsername());
        String accessToken = jwtTokenProvider.generateAccessToken(
                username,
                password
        );
        String refreshToken = jwtTokenProvider.generateRefreshToken(accessToken, password);

        authResponseModel.setAccessToken(accessToken);
        authResponseModel.setRefreshToken(refreshToken);
        authResponseModel.setExpiresIn(jwtTokenProvider.getAccessTokenExpirationMs());
        return authResponseModel;
    }

    @Override
    public AuthResponseDTO getAuthResponseDTO() {
        AuthRequestDTO authRequestDTO = new AuthRequestDTO();
        authRequestDTO.setLogin(getAuthenticatedUserDetails().getUsername());
        authRequestDTO.setPassword(getAuthenticatedUserDetails().getPassword());
        return createAuthorizationResponse(authRequestDTO);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUserDTO(String username) {
        return userService.getByUsername(username);
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getUserDTO() {
        UserDetails userDetails = getAuthenticatedUserDetails();
        return userService.getByUsername(userDetails.getUsername());
    }


    @Override
    @Transactional(readOnly = true)
    public String getUsername() {
        return getAuthenticatedUserDetails().getUsername();
    }

    @Override
    public UserEntity getUserEntity() {
        UserDetails userDetails = getAuthenticatedUserDetails();
        return userService.findByUsername(userDetails.getUsername());
    }

    @Override
    public boolean jwtHasReset(String token) {
        return jwtTokenProvider.jwtHasReset(jwtTokenProvider.parseJwt(token));
    }

    @Override
    @Transactional(readOnly = true)
    public AuthResponseDTO renew(String refreshToken) {
        String jwt = jwtTokenProvider.parseJwt(refreshToken);
        if (!refreshTokenService.exists(jwt)) {
            throw new RefreshTokenNotFoundException("Refresh token not found");
        }
        refreshTokenService.delete(jwt);
        jwtTokenProvider.validateJwtTokenSignature(jwt);
        jwtTokenProvider.validateJwtToken(jwt, getPasswordByUsername(getUsername()));
        return getAuthResponseDTO();
    }

    @Override
    public boolean isOwner(Long createdById) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();
        return userDetails.getUsername().equals(userService.getUsernameById(createdById));
    }

    private String getPasswordByUsername(String username) {
        return userService.findByUsername(username).getPassword();
    }

    private UserDetails getAuthenticatedUserDetails() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return (UserDetails) authentication.getPrincipal();
    }
}
