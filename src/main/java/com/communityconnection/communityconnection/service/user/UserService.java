package com.communityconnection.communityconnection.service.user;

import com.communityconnection.communityconnection.jpa.domain.user.UserEntity;
import com.communityconnection.communityconnection.service.user.dto.BaseUserDTO;
import com.communityconnection.communityconnection.service.user.dto.UserDTO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;

public interface UserService {

    Long create(BaseUserDTO createUserDTO);

    UserDTO getByUsername(String username);

    UserEntity findByUsername(String username);

    void delete(List<Long> ids);

    UserEntity findById(Long userId);

    boolean existsByUsername(String username);

    String findByEmail(String email);

    Page<UserDTO> getAll(PageRequest pageRequest);

    Page<UserDTO> getAll(String searchBy, PageRequest pageRequest);

    String getUsernameById(Long createdById);
}
