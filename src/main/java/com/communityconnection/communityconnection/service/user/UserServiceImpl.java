package com.communityconnection.communityconnection.service.user;

import com.communityconnection.communityconnection.config.mapping.DTOMapper;
import com.communityconnection.communityconnection.exception.nonunique.AlreadyTakenException;
import com.communityconnection.communityconnection.jpa.domain.user.UserEntity;
import com.communityconnection.communityconnection.jpa.repository.user.UserRepository;
import com.communityconnection.communityconnection.service.mail.EmailSenderService;
import com.communityconnection.communityconnection.service.user.dto.BaseUserDTO;
import com.communityconnection.communityconnection.service.user.dto.UserDTO;
import com.communityconnection.communityconnection.service.utils.FindOne;
import com.communityconnection.communityconnection.service.utils.SoftDeleteService;
import com.communityconnection.communityconnection.utils.EntityName;
import com.communityconnection.communityconnection.utils.validators.password.PasswordUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@RequiredArgsConstructor
public class UserServiceImpl extends SoftDeleteService implements UserService {

    private final EmailSenderService emailSenderService;
    private final UserRepository userRepository;
    private final DTOMapper dtoMapper;
    private final FindOne findOne;

    @Override
    @Transactional
    public Long create(BaseUserDTO createUserDTO) {
        checkUniqueFields(createUserDTO);
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername(createUserDTO.getUsername());
        userEntity.setFirstName(createUserDTO.getFirstName());
        userEntity.setLastName(createUserDTO.getLastName());
        userEntity.setMiddleName(createUserDTO.getMiddleName());
        userEntity.setEmail(createUserDTO.getEmail());
        userEntity.setPhoneNumber(createUserDTO.getPhoneNumber());

        String oneTimePassword = PasswordUtils.generate();
        userEntity.setPassword(oneTimePassword);
        userRepository.save(userEntity);

        emailSenderService.sendCreatePasswordEmail(createUserDTO, oneTimePassword);
        return userEntity.getId();
    }

    @Override
    @Transactional
    public void delete(List<Long> ids) {
        deleteExtendedFromBaseEntity(ids, EntityName.USER, UserEntity.class);
    }

    @Override
    @Transactional(readOnly = true)
    public UserEntity findById(Long userId) {
        return findOne.getUser(userId);
    }

    @Override
    @Transactional(readOnly = true)
    public boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    @Transactional(readOnly = true)
    public String findByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(() ->
                new AuthenticationCredentialsNotFoundException("Invalid login or password")).getUsername();
    }

    private void checkUniqueFields(BaseUserDTO createUserDTO) {
        if (userRepository.existsByUsername(createUserDTO.getUsername()))
            throw new AlreadyTakenException("Username", createUserDTO.getUsername());
        if (userRepository.existsByPhoneNumber(createUserDTO.getPhoneNumber()))
            throw new AlreadyTakenException("Phone number", createUserDTO.getPhoneNumber());
        if (userRepository.existsByEmail(createUserDTO.getEmail()))
            throw new AlreadyTakenException("Email", createUserDTO.getEmail());
    }

    @Override
    @Transactional(readOnly = true)
    public UserDTO getByUsername(String username) {
        return dtoMapper.toDTO(getUser(username));
    }

    @Override
    @Transactional(readOnly = true)
    public UserEntity findByUsername(String username) {
        return getUser(username);
    }

    private UserEntity getUser(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException(username));
    }

    @Override
    @Transactional
    public Page<UserDTO> getAll(PageRequest pageRequest) {
        Page<UserEntity> userEntities = userRepository.findAll(pageRequest);
        return userEntities.map(dtoMapper::toDTO);
    }

    @Override
    @Transactional
    public Page<UserDTO> getAll(String searchBy, PageRequest pageRequest) {
        return null;
    }

    @Override
    public String getUsernameById(Long createdById) {
        UserEntity creator = findOne.getUser(createdById);
        return creator.getUsername();
    }

}
