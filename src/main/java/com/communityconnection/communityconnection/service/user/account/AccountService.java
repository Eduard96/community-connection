package com.communityconnection.communityconnection.service.user.account;


import com.communityconnection.communityconnection.service.user.account.dto.ChangePasswordDTO;
import com.communityconnection.communityconnection.service.user.dto.CreatePasswordUserDTO;

public interface AccountService {
    void updatePassword(CreatePasswordUserDTO createPasswordUserDTO);

    void sendResetPasswordEmail(String email);

    void updatePassword(ChangePasswordDTO changePasswordDTO);

}
