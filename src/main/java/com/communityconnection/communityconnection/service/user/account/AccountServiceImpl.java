package com.communityconnection.communityconnection.service.user.account;

import com.communityconnection.communityconnection.exception.resourcenotfound.ResourceNotFoundException;
import com.communityconnection.communityconnection.jpa.domain.user.UserEntity;
import com.communityconnection.communityconnection.jpa.repository.user.UserRepository;
import com.communityconnection.communityconnection.service.mail.EmailSenderService;
import com.communityconnection.communityconnection.service.security.SecurityService;
import com.communityconnection.communityconnection.service.user.account.dto.ChangePasswordDTO;
import com.communityconnection.communityconnection.service.user.dto.CreatePasswordUserDTO;
import com.communityconnection.communityconnection.utils.EntityName;
import com.communityconnection.communityconnection.utils.validators.password.PasswordUtils;
import lombok.RequiredArgsConstructor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private static final Logger LOGGER = LogManager.getLogger(AccountServiceImpl.class);

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final EmailSenderService emailSenderService;
    private final SecurityService securityService;

    @Override
    @Transactional
    public void updatePassword(CreatePasswordUserDTO createPasswordUserDTO) {
        PasswordUtils.comparePasswords(createPasswordUserDTO);
        updatePassword(createPasswordUserDTO.getPassword());
    }

    @Override
    public void sendResetPasswordEmail(String email) {
        UserEntity userEntity = userRepository.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(EntityName.USER, "email", email));
        emailSenderService.sendResetPasswordEmail(email, userEntity.getUsername(), userEntity.getPassword());
    }

    @Override
    @Transactional
    public void updatePassword(ChangePasswordDTO changePasswordDTO) {
        PasswordUtils.comparePasswords(changePasswordDTO);
        updatePassword(changePasswordDTO.getNewPassword1());
    }

    private void updatePassword(String password) {
        UserEntity userEntity = userRepository
                .findByUsername(securityService.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException(EntityName.USER, "username", securityService.getUsername()));
        userEntity.setPassword(passwordEncoder.encode(password));
        userRepository.save(userEntity);
    }
}
