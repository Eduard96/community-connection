package com.communityconnection.communityconnection.service.user.account.dto;

import com.communityconnection.communityconnection.utils.validators.password.ValidatePassword;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Getter
@Setter
public class AuthRequestDTO {

    @NotEmpty
    @NotBlank
    private String login;

    @ValidatePassword
    private String password;
}
