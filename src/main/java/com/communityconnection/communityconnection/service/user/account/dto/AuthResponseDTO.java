package com.communityconnection.communityconnection.service.user.account.dto;

import com.communityconnection.communityconnection.service.user.dto.UserDTO;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class AuthResponseDTO {

    private final UserDTO user;

    public AuthResponseDTO(UserDTO user) {
        this.user = user;
    }

    @NotEmpty
    private String accessToken;

    @NotEmpty
    private String refreshToken;

    @NotNull
    private Long expiresIn;

}
