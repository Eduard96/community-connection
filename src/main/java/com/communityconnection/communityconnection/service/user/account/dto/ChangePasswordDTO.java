package com.communityconnection.communityconnection.service.user.account.dto;

import com.communityconnection.communityconnection.utils.validators.password.ValidatePassword;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangePasswordDTO {

    @ValidatePassword
    private String oldPassword;

    @ValidatePassword
    private String newPassword1;

    @ValidatePassword
    private String newPassword2;
}
