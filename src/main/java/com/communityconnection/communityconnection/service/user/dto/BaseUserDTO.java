package com.communityconnection.communityconnection.service.user.dto;


import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class BaseUserDTO {

    @NotEmpty
    private String username;
    @NotEmpty
    private String firstName;
    @NotEmpty
    private String lastName;

    private String middleName;
    @NotEmpty
    private String phoneNumber;
    @Email
    private String email;
    @NotNull
    private Long roleId;
}
