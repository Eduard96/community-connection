package com.communityconnection.communityconnection.service.user.dto;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SimpleUserDTO {
    private Long id;
    private String firstName;
    private String lastName;

}
