package com.communityconnection.communityconnection.service.user.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
public class UserDTO extends BaseUserDTO {

    @NotNull
    private Long id;

    @NotNull
    private LocalDateTime lastModifiedDate;

    @NotNull
    private Long lastModifiedBy;

    @NotNull
    private LocalDateTime createdDate;

    @NotNull
    private Long createdBy;
}
