package com.communityconnection.communityconnection.service.user.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class UserFilterDTO {

    private String username;
    
    private String firstName;
    
    private String lastName;
    
    private String middleName;
    
    private String phoneNumber;

    private String email;

    private Long roleId;
}
