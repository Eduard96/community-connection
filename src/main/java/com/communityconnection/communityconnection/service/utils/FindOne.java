package com.communityconnection.communityconnection.service.utils;

import com.communityconnection.communityconnection.exception.resourcenotfound.ResourceNotFoundException;
import com.communityconnection.communityconnection.jpa.domain.booking.BookingEntity;
import com.communityconnection.communityconnection.jpa.domain.bookspace.BookingSpaceEntity;
import com.communityconnection.communityconnection.jpa.domain.user.UserEntity;
import com.communityconnection.communityconnection.jpa.repository.booking.BookingRepository;
import com.communityconnection.communityconnection.jpa.repository.bookingspace.BookingSpaceRepository;
import com.communityconnection.communityconnection.jpa.repository.user.UserRepository;
import com.communityconnection.communityconnection.utils.EntityName;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class FindOne {

    private final UserRepository userRepository;
    private final BookingRepository bookingRepository;
    private final BookingSpaceRepository bookingSpaceRepository;

    public UserEntity getUser(Long userId) {
        return userRepository
                .findById(userId)
                .orElseThrow(() -> new ResourceNotFoundException(EntityName.USER, userId));
    }

    public BookingEntity getBooking(Long bookingId) {
        return bookingRepository
                .findById(bookingId)
                .orElseThrow(() -> new ResourceNotFoundException(EntityName.BOOKING, bookingId));
    }

    public BookingSpaceEntity getBookingSpace(Long bookingSpaceId) {
        return bookingSpaceRepository
                .findById(bookingSpaceId)
                .orElseThrow(() -> new ResourceNotFoundException(EntityName.BOOKING_SPACE, bookingSpaceId));
    }
}
