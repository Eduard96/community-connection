package com.communityconnection.communityconnection.service.utils;

import com.communityconnection.communityconnection.exception.SoftDeleteQueryBuilderException;
import com.communityconnection.communityconnection.jpa.domain._BaseEntity;
import org.apache.commons.lang3.StringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.persistence.Column;
import java.lang.reflect.Field;
import java.util.List;

public class SoftDeleteQueryBuilder {

    private static final Logger LOGGER = LogManager.getLogger(SoftDeleteQueryBuilder.class);

    private Class<?> entity;
    private StringBuilder query;
    private String concat; 
    private String where;


    /**
     * Build sql query like
     * "UPDATE table SET " +
     *         "deleted = true, " +
     *         "unique_field_1 = CONCAT(unique_field_1, '_deleted_', CURRENT_TIMESTAMP), " +
     *         "unique_field_2 = CONCAT(unique_field_2, '_deleted_', CURRENT_TIMESTAMP), " +
     *         "and so on " +
     *         "WHERE deleted = false and id in (1, 2, 3, ...)"
     */
    public <T extends _BaseEntity> String  getDeleteQueryByIdsExtendedFromBaseEntity(List<Long> ids, String entityName, Class<T> entity) {
        this.entity = entity;
        query = new StringBuilder("UPDATE " + entityName + " SET deleted = true,");
        concat = " %s = CONCAT(%s, '_deleted_', CURRENT_TIMESTAMP),";
        where = " WHERE deleted = false AND id in (%s)";
        return getQuery(ids);
    }

    /**
     * Build sql query like
     * "UPDATE table SET " +
     *         "unique_field_1 = CONCAT(unique_field_1, '_deleted_', CURRENT_TIMESTAMP), " +
     *         "unique_field_2 = CONCAT(unique_field_2, '_deleted_', CURRENT_TIMESTAMP), " +
     *         "and so on " +
     *         "WHERE id in (1, 2, 3, ...)"
     */
    public <T> String getDeleteQueryByIdsForChild(List<Long> ids, String entityName, Class<T> entity) {
        this.entity = entity;
        query = new StringBuilder("UPDATE " + entityName + " SET");
        concat = " %s = CONCAT(%s, '_deleted_', CURRENT_TIMESTAMP),";
        where = " WHERE id in (%s)";
        return getQuery(ids);
    }
    
    private String getQuery(List<Long> ids) {
        String completeQuery = null;
        try {
            Field[] fields = entity.getDeclaredFields();
            for (Field field : fields) { 
                field.setAccessible(true);
                Column column = field.getAnnotation(Column.class);
                if (field.getType().isAssignableFrom(String.class) && column.unique()) {
                    //regex to replace field name from camel case to snake case
                    String fieldName = toSnakeCase(field.getName());
                    query.append(String.format(concat, fieldName, fieldName));
                }
            }
            //substring without last comma from CONCAT
            completeQuery = query.substring(0, query.length() - 1);
            completeQuery += String.format(where, StringUtils.join(ids, ','));
            LOGGER.info(completeQuery);
            return completeQuery;
        } catch (Exception ex) {
            LOGGER.error(completeQuery, ex.getMessage());
            throw new SoftDeleteQueryBuilderException("Something went wrong");
        }
    }

    private String toSnakeCase(String str) {
        return str.replaceAll("([^_A-Z])([A-Z])", "$1_$2").toLowerCase();
    }
}
