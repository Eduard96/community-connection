package com.communityconnection.communityconnection.service.utils;


import com.communityconnection.communityconnection.jpa.domain._BaseEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

@Component
public class SoftDeleteService {

    private static final Logger LOGGER = LogManager.getLogger(SoftDeleteService.class);

    @PersistenceContext
    private EntityManager entityManager;

    public <T extends _BaseEntity> void deleteExtendedFromBaseEntity(List<Long> ids, String entityName, Class<T> cls) {
        SoftDeleteQueryBuilder queryBuilder = new SoftDeleteQueryBuilder();
        Query query = entityManager.createNativeQuery(queryBuilder.getDeleteQueryByIdsExtendedFromBaseEntity(ids, entityName, cls));
        int code = query.executeUpdate();
        LOGGER.info("update code: {}", code);
    }

    public <T> void deleteForChild(List<Long> ids, String entityName, Class<T> cls) {
        SoftDeleteQueryBuilder queryBuilder = new SoftDeleteQueryBuilder();
        Query query = entityManager.createNativeQuery(queryBuilder.getDeleteQueryByIdsForChild(ids, entityName, cls));
        int code = query.executeUpdate();
        LOGGER.info("update code: {}", code);
    }
}
