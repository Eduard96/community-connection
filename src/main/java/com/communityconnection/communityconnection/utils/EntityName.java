package com.communityconnection.communityconnection.utils;

public class EntityName {

    public static final String REFRESH_TOKEN = "refresh_token";
    public static final String USER = "user";
    public static final String COMMUNITY_USER = "community_user";
    public static final String PARKING_SPACE = "parking_space";
    public static final String BOOKING_SPACE = "booking_space";
    public static final String BOOKING = "booking";
    public static final String COWORKING_SPACE = "coworking_space";
}
