package com.communityconnection.communityconnection.utils.booking;

public enum BookingSpaceType {

    PARKING_SPACE,
    COWORKING_SPACE;
}
