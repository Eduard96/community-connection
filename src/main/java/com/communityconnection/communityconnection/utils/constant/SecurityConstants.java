package com.communityconnection.communityconnection.utils.constant;

public class SecurityConstants {

    public static final String TOKEN_TYPE = "Bearer ";

    public static final String ROLE_ADMIN = "ROLE_ADMIN";

    public static final String ROLE_USER = "ROLE_USER";

    public static final String PASSWORD = "password";

    public static final int TEMP_PASSWORD_LENGTH = 15;
}
