package com.communityconnection.communityconnection.utils.pageandsort;

import org.springframework.data.domain.Sort;

/**
 * General class for sorting by properties with pagination
 */
public class ImplSort {

    public static Sort by(String sortProp, Sort.Direction sortDirection) {
        return Sort.by(sortDirection, sortProp, "id");
    }

}
