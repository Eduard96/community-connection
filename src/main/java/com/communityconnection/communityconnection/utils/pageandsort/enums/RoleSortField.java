package com.communityconnection.communityconnection.utils.pageandsort.enums;

public enum RoleSortField {

    ID("id"),
    NAME("name");

    private final String name;

    RoleSortField(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
