package com.communityconnection.communityconnection.utils.pageandsort.enums;

public enum UserSortField {

    ID("id"),
    USERNAME("username"),
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"),
    PHONE_NUMBER("phoneNumber"),
    CREATED_DATE("createdDate"),
    ROLE_ID("role.id"),
    ROLE_NAME("role.name");

    private final String name;

    UserSortField(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
