package com.communityconnection.communityconnection;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class CommunityConnectionApplicationTests {

	@Test
	void contextLoads() {
		//if you have no tests, so you have no test fails :D
	}

}
